export class Monster {
    name: string;
    hp: number;
    maxHp: number;
    def: number;
    annotation = '';
    provocationPlayer = '';

    constructor(name: string, hp: number, def: number) {
        this.name = name;
        this.hp = hp;
        this.maxHp = hp;
        this.def = def;
    }
}
