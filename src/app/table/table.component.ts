import { AfterViewInit, Component, OnInit, ViewChild, Input } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';
import { Monster } from './monster';
import { Subject } from 'rxjs';
import { Player } from './player';
import {DomSanitizer} from '@angular/platform-browser';

import {MatIconRegistry} from '@angular/material/icon';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements AfterViewInit {


  @Input() channelID: string;

  addMobForm: FormGroup;
  addPlayerForm: FormGroup;
  monsters: Monster[] = [];
  players: Player[] = [];


  dtTrigger: Subject<void> = new Subject();
  displayedColumns: string[] = ['name', 'hp'];

  constructor(private formBuilder: FormBuilder, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, private http: HttpClient) {
    this.addMobForm = formBuilder.group({
      name: '',
      hp: 0,
      number: 0,
      def: 0,
    });
    this.addPlayerForm = formBuilder.group({
      name: ''
    });

    iconRegistry.addSvgIcon(
      'delete',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icons8-delete.svg'));
  }

  ngAfterViewInit() {
    this.http.post('http://62.4.21.196:8080/init', {channel: this.channelID},
    {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }).subscribe((res) => {
      console.log(res);
    });
  }


  onSubmitMob(values: any): void {

    if (isNaN(Number(values.number)) || isNaN(Number(values.hp)) || values.hp <= 0) { return; }

    for (let i = 0; i < values.number; i++) {
      this.monsters.push(new Monster(values.name, Number(values.hp), Number(values.def)));
    }
    this.addMobForm.reset();
    console.log(this.monsters);

  }
  onSubmitPlayer(values: any): void {

    this.players.push(new Player(values.name));
    this.addPlayerForm.reset();

  }

  removePlayer(i: number): void {
    this.players.splice(i, 1);
  }

  damageMob(i: number, damage: HTMLInputElement, ignoreDef: boolean): void {
    if (isNaN(Number(damage.value)) || Number(damage.value) === 0) { return; }
    let d = Number(damage.value);
    if (this.monsters[i].def > 0 && d > 0 && !ignoreDef) {
      d -= this.monsters[i].def;
    }
    this.monsters[i].hp = Math.max(0, Math.min(this.monsters[i].maxHp, this.monsters[i].hp - d));
  }

  provoCheck(i: number, checked: boolean): void {
    this.players[i].provocating = checked;
  }

  provoMob(mobIndex: number, prov: boolean, playerName: string) : void {
    this.monsters[mobIndex].provocationPlayer = prov ? playerName : '';
  }

  annoteMob(i: number, annotation: string): void {
    this.monsters[i].annotation = annotation;
  }

  annotePlayer(i: number, annotation: string): void {
    this.players[i].annotation = annotation;
  }


  sendTable(): void {



    this.http.post('http://62.4.21.196:8080/sendTable', {channel: this.channelID, monsters: this.monsters, players: this.players},
    {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }).subscribe((res) => {
      console.log(res);
    });
  }

}
